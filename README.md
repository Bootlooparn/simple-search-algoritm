# Simple-search-algorithm

A search algorithm that works in the following way.

Reads an arbitrary amount of documents, .txt, and maps each word in the form of an inverted index with the following structure:

$`W = \{w | w \in d\}`$

$`w = \{d| d \in D \}`$

$`d = \{ i | w_i \in d \}`$

Where $`D`$ represents the corpus i.e. document collection, $`w`$ represents a word in a document $`d`$ and $`i`$ represents the index of the word in a document.

Words are then matched with their respective documents and said documents are then ordered by their TF-IDF score. The TF-IDF score are calcuated in the following fashion:

$`TF=\cfrac{f_{t,d}}{\sum f_{t', d}}`$

$`IDF=log_2(1 + \cfrac{|D|}{n_t})`$

$`n_t = |d \in D : t \in d|`$

$`TF`$ is essentially the word count in the document divided by the total count of words in the document. Where as the inner fraction in IDF is the total count of documents in the corpus divided by the number of documents the word appears in.

The score is then calculated for each document:

$`TF*IDF`$ and sorted there after in descending order.