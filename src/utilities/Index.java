package utilities;

import java.util.Arrays;

public class Index {
    private String document;
    private int[] value;

    public Index (String d, int [] o) {
        document = d;
        value = o;
    }

    public void add (int i){
        value = Arrays.copyOf(value, value.length + 1);
        value[value.length - 1] = i;
    }

    public String getDocumentName () {
        return document;
    }

    public int[] getValues () {
        return value;
    }
}
