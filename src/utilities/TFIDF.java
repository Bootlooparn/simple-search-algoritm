package utilities;

public class TFIDF {
    public static double TF(double n, double d) {
        double tf = n/d;
        return tf;
    }
    public static double IDF(double n, double d) {
        double idf;
        idf = n/d;
        idf = Math.log(1+idf);
        return idf;
    }
}
