package utilities;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;


public class Indexer {
    private File[] files;
    private HashMap masterIndex = new HashMap<String, ArrayList<Index>>();
    private LinkedHashMap termFrequency = new LinkedHashMap<String, Integer>();

    public Indexer (File[] f){
        files = f;
    }

    private void indexFile (File file) throws IOException {
        String filename = file.getName();
        Stream<String> stream = Files.lines(Paths.get(file.toString()));
        List<String> list = stream
                .map(line -> line.replaceAll("[^a-öA-Ö0-9 ]", ""))
                .map(String::toLowerCase)
                .filter(line -> !line.isEmpty())
                .flatMap(Pattern.compile("\\s")::splitAsStream)
                .collect(Collectors.toList());

        termFrequency.put(filename, list.size());

        int i = 0;
        for (String w: list) {
            if (!masterIndex.containsKey(w)) {
                Index node = new Index(filename, new int[] {i});
                List<Index> nodeList = new ArrayList<>(Collections.singletonList(node));
                masterIndex.put(w, nodeList);
            } else {
                List<Index> nodeList = (List<Index>) masterIndex.get(w);
                if (nodeList.stream().noneMatch(n -> n.getDocumentName() == filename)) {
                    Index node = new Index(filename, new int[] {i});
                    nodeList.add(node);
                    masterIndex.put(w, nodeList);
                } else {
                    int j = 0;
                    while (filename != nodeList.get(j).getDocumentName()) {
                        j++;
                    }
                    nodeList.get(j).add(i);
                    masterIndex.put(w, nodeList);
                }
            }
            i++;
        }
    }

    private Map<String, Double> sortDocuments (String w) {
        Map<String, Double> matchingDocuments = new LinkedHashMap<>();

        try {
            List<Index> node = (List<Index>) masterIndex.get(w);
            Map<String, Double> sortingDoc = new HashMap<>();

            for (Index n: node) {
                Integer tfd = (Integer) termFrequency.get(n.getDocumentName());

                double tf = TFIDF.TF(n.getValues().length, tfd.doubleValue());
                double idf = TFIDF.IDF(termFrequency.size(), node.size());

                sortingDoc.put(n.getDocumentName(), tf*idf);
            }
            matchingDocuments = sortingDoc.entrySet().stream()
                    .sorted(Map.Entry.<String, Double>comparingByKey().reversed())
                    .sorted(Map.Entry.<String, Double>comparingByValue().reversed())
                    .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue, (a, b) -> a, LinkedHashMap::new));
        } catch (Exception e) {
            System.out.println("No such term exists");
        }
        return matchingDocuments;
    }

    public void masterIndex () throws Exception {
        for (File file: files) {
            indexFile(file);
        }
    }

    public Map<String, Double> queryWord (String query) {
        Map<String, Double> documents = sortDocuments(query.toLowerCase());
        return documents;
    }
}
