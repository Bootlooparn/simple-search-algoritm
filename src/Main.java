import javax.swing.*;
import java.io.BufferedReader;
import java.io.File;
import java.io.InputStreamReader;
import java.util.*;
import java.util.AbstractMap.SimpleEntry;

import utilities.Indexer;

public class Main {

    public static void main(String[] args) throws Exception {
        String cwd = System.getProperty("user.dir");

        JFileChooser dialog = new JFileChooser(cwd);
        dialog.setMultiSelectionEnabled(true);

        dialog.showSaveDialog(null);
        File[] files = dialog.getSelectedFiles();

        Indexer index = new Indexer(files);
        index.masterIndex();

        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String term = reader.readLine();

        while (!(term.isEmpty())) {
            Map<String, Double> queryReturn = index.queryWord(term);

            if (!queryReturn.isEmpty()) {
                System.out.println(queryReturn.keySet());
                System.out.println(queryReturn.values());
            }
            term = reader.readLine();
        }
        System.out.println("Goodbye");
    }
}
